# SQLSolutions: Requirements and Specification Document

[![pipeline status](https://gitlab.com/sqlsolutions/mysql-file-system/badges/main/pipeline.svg)](https://gitlab.com/sqlsolutions/mysql-file-system/-/pipelines)


[![coverage report](https://gitlab.com/sqlsolutions/mysql-file-system/badges/main/coverage.svg)](https://gitlab.com/sqlsolutions/mysql-file-system/-/commits/main)

## Group Members
Aidan Yim <br/>
Ryan Okushi <br/>
Zian Fang <br/>
Saswat Nayak <br/>
Jinwoong Shin <br/>
Samuel Knopes <br/>

## Project Abstract

In this project, we seek to develop an efficient file system by utilizing a relational database as the underlying storage system. The system will feature a user-friendly web interface enabling users to seamlessly perform basic file system operations in addition to visualizing directories as tree structures. Within its database, the project will store the structure of files and directories, along with metadata such as permissions and creation/update dates. The web application will incorporate user login functionality, ensuring secure management of file system permissions.

## Customer

The ideal customer that this project targets would include anyone dealing with user permissions (like IT departments or system administrators), cloud service providers that seek to store files and their metadata efficiently, and small businesses looking for user-friendly file management system that is customizable.

## User Requirements

<!--This section lists the behavior that the users see. This information needs to be presented in a logical, organized fashion. It is most helpful if this section is organized in outline form: a bullet list of major topics (e.g., one for each kind of user, or each major piece of system functionality) each with some number of subtopics.-->

| ID   | Description                                                  | Priority | Status |
| ---- | ------------------------------------------------------------ | -------- | ------ |
| 1  | The system must allow the user to create a file. | High     | Closed   |
| 2  | The system must allow the user to update the title and contents of any file they have permission to write to. | High     | Closed   |
| 3  | The system must allow the user to delete any file they have permission to write to. | High     | Closed   |
| 4  | The system must allow the user to view any file they have permission to read. | High     | Closed   |
| 5  | The system must allow the user to create a directory. | High     | Closed   |
| 6  | The system will present files and directories in human readable fashion. | High     | Closed   |
| 7  | The system will allow administrative users to assign file permissions to users. | Medium      | Closed   |
| 8  | The system will bar users from editing files they do not have the write permission for. | Medium      | Closed   |
| 9  | The system will bar users from deleting files they do not have the write permission for. | Medium      | Closed   |
| 10  | The system will bar users from viewing files they do not have the read permission for. | Medium      | Closed   |
| 11  | The system will allow the relocation of a file or directory from one directory to another. | Medium      | Closed   |
| 12  | The system will allow the copying of a file or directory from one directory to another. | Medium      | Open   |
| 13  | The system will prevent the creation of two files or directories of the same name in the same directory. | Medium     | Open   |
| 14  | The system will prevent the copying or relocation of a file or directory if there already exists another file or directory with the same name in the target directory. | Medium     | Open   |
| 15  | The system will allow new users to create a new login with a unique username. | Low      | Open   |
| 16  | The system will place deleted files or directories into a "recently deleted" folder or "trash bin." | Low      | Open   |
| 17  | The list of files & directories will not be displayed until the user has logged in. | Low     | Closed   |
| 18  | The system will allow users to logout of an account. | Low     | Open   |
| 19  | File changes made by the user will persist between login sessions. | Low     | Closed   |

## User Stories

<!--Use cases and user stories that support the user requirements in the previous section. The use cases should be based off user stories. Every major scenario should be represented by a use case, and every use case should say something not already illustrated by the other use cases. Diagrams (such as sequence charts) are encouraged. Ask the customer what are the most important use cases to implement by the deadline. You can have a total ordering, or mark use cases with “must have,” “useful,” or “optional.” For each use case you may list one or more concrete acceptance tests (concrete scenarios that the customer will try to see if the use case is implemented).-->

1. As a user of the filesystem, in order to store information on my device, I will create a new file for it. (must have)

2. As a user of the filesystem, in order to access information I've already stored, I will open and read an existing file. (must have)

3. As a user of the filesystem, in order to keep my files up to date, I will edit the contents of an existing file. (must have)

4. As a user of the filesystem, in order to keep my device clutter-free, I will delete files I no longer require. (must have)

5. As a user of the filesystem, in order to locate the file I'm looking for, I will refer to a visualization or list of the file directories. (must have)

6. As a user of the filesystem, I can click on a directory or file to view its contents (must have)

7. As a user of the filesystem, in order to organize my files, I will create a new directory for them. (useful)

8. As a user of the filesystem, in order to begin creating and managing files, I will create an account. (useful)

9. As a user of the filesystem, in order to access my private files, I will login to my existing account. (useful)

10. As a user of the filesystem, in order to prevent others from accessing my files, I will log out of my account. (useful)

11. As a user of the filesystem, in order to share my files at my will, I will be able to modify permissions for contents I have full admin access to (useful)

12. As a user of the filesystem, in order to store my files remotely, I will be able to upload a new file that contains text (useful)

13. As a user of the filesystem, in order to easily keep track of the file contents, I will edit the title of an existing file. (optional)

14. As a user of the filesystem, in order to keep my device organized, I will edit the locations/directories of existing files. (optional)

15. As a user of the filesystem, in order to back up my personal data, I will create a copy of an existing file and be able to paste it (or cut it). (optional)

16. As a user of the filesystem, in order to retrieve some files I accidentally deleted, I will view my recently deleted files or directories in a trash folder (optional)

17. As a user of the filesystem, in order for me to better understand, these actions will be executed in a user friendly UI that resembles the file system in an operating system. (optional)

## Test Plans
<!--Determines what tests need to be written in the end. The numbers are mapped to the user requirements.-->

### 1. Create a file
Given logged in as a user.  
Given user has permission to write in the current directory.  
Create a file using the create method  
Success: file is created, added to the database and marked as file.

### 2. Update title and content
Given logged in as a user.  
Given a file that user has permission to write.  
Update the title.  
Update the content.  
Success: the title and content in the database are changed.

### 3. Delete a file
Given logged in as a user.  
Given a file that user has permission to write.  
Delete the file.  
Success: the file entry is no longer in the database and the contents are removed.

### 4. View a file
Given logged in as a user.  
Given a file that user has permission to read.  
Open the file.  
Success: the content of the file is returned.

### 5. Create a directory
Given logged in as a user.  
Given user has permission to write in the current directory.  
Create a directory using the create method.  
Success: directory is created, added to the database and marked as directory.

### 6. Assign permission
Given logged in as admin.  
Given a test file.  
Create a user without any permission to the file.  
Assign file read permission and write permission to that user.  
Log in as the user.  
Success: reading the file returns the content and writing to the file changes its contents.

### 7. Bar users from editing
Given logged in as a user.  
Given a test file that the current account has no permission to write.  
Success: attempt to edit the file fails and the contents of the file are not changed.

### 8. Bar users from deleting
Given logged in as a user.  
Given a test file that the current account has no permission to write.  
Success: attempt to delete the file fails and the contents of the file are not changed.

### 9. Bar users from viewing
Given logged in as a user.  
Given a test file that the current account has no permission to read.  
Success: attempt to read the file content fails and the contents are not returned.

### 10. Trash bin
Given logged in as a user.  
Given a file that user has permission to write.  
Delete the file.  
Success: the deleted file is in the "trash bin" zone and the contents are preserved.

### 11. Relocation
Given logged in as a user.  
Given user has permission to read from and write to the current directory and write to the target directory.  
Relocate the file from the current directory to the target directory.  
Success: the file's hierarchy is correctly changed to reflect the relocation, while its contents remain the same.

### 12. Copying
Given logged in as a user.  
Given user has permission to read from the current directory and write to the target directory.  
Copy the file from the current directory to the target directory.  
Success: a new file in the target directory is created with the same name and content as the original file, while the original file remains unchanged.

### 13. Prevent same name creation
Given logged in as a user.  
Given user has permission to read from and write to the current directory.  
Given a file.  
Attempt to create another file with the same name.  
Success: the attempt fails, the original file is not changed, the new file does not appear in the database.

### 14. Prevent same name relocation and copying
Given logged in as a user.  
Given user has permission to read from and write to the current directory and write to the target directory.  
Given two files with the same name in the current directory and the target directory.  
Attempt to relocate the file from the current directory to the target directory.  
Attempt to copy the file from the current directory to the target directory.  
Success: both attempts fail, the two files are not changed.

### 15. Create account
Create a new account with an unique username.  
Success: the username+password pair is added to database, login with the username+password is now successful.

### 16. Hide content until login
Given not logged in.  
Attempt to read the file list of the current directory.  
Log in as a user.  
Read the file list of the current directory.  
Success: the first attempt fails and the second attempt successes.

### 17. Log out
Given logged in as a user.  
Log out.  
Attempt the read the file list of the current directory.  
Success: the attempt fails.

### 18. File changes persist
Given logged in as a user.  
Given a file that user has permission to read from and write to.  
Edit the content of the file.  
Log out.  
Log in as the same user.  
Read the content of the file.  
Success: the content of the file remains changed.

## Use Case Diagram

![use case diagram](figures/CS506_Use_Case_Diagram.png)

The user logs in, creating a new account if new, and is brought to the main menu which serves as a visualization of the hierarchical filesystem. From there, the user can either create a new file or perform any of several options upon an existing file, including viewing/reading, updating, and deleting.

### User Interface Requirements
The user should be able to clearly see all CRUD operations that they can do in the interface as well as visualization of a directory and what directory they are currently in. Any optional additional functionalities that are implemented will also be shown here. 

## Security Requirements

<!--Discuss what security requirements are necessary and why. Are there privacy or confidentiality issues? Is your system vulnerable to denial-of-service attacks?-->

The system will ensure that only users with the correct permissions can access the files they have the read or write permissions for. This will allow for more capable system administration. 

## System Requirements

<!--List here all of the external entities, other than users, on which your system will depend. For example, if your system inter-operates with sendmail, or if you will depend on Apache for the web server, or if you must target both Unix and Windows, list those requirements here. List also memory requirements, performance/speed requirements, data capacity requirements, if applicable.-->

As of now, we do not foresee any other entities on which our system will depend. Our system will have performance/speed requirement of giving the user their request within a timely manner (2 seconds or less) if possible.

## Specification

<!--A detailed specification of the system. UML, or other diagrams, such as finite automata, or other appropriate specification formalisms, are encouraged over natural language.-->

<!--Include sections, for example, illustrating the database architecture (with, for example, an ERD).-->

### System Architecture
![database architecture backend](figures/system_architecture_backend.png)

### Database
![database architecture](figures/UPDATED_DB_WITH_USERS.png)

![database ER diagram](figures/506_ER_Diagram.png)

### Technology Stack (PYTHON):

##### 1.Web Application Framework Decision:
We chose <img src="https://img.shields.io/badge/Flask-000000?style=for-the-badge&logo=flask&logoColor=white"/>  over Django becasue Flask provides more flexibility and is lightweight, which is advantageous for building a simpler, more controlled application. Flask allows us to add only the components we need, avoiding the overhead of Django's more monolithic structure. This leads to clearer and more concise codebase which is particularly beneficial for smaller-scale projects or when starting with a microstrucutre architecture. Bascially, Flask is simpler than Django.

##### 2. Frontend Development Tools:
We are sticking with basic web technologies like <img src="https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white"/> , <img src ="https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white"> , <img src="	https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E"> because they form the foundation of web development and are sufficient for constructing our application's user interface. This choice allows us to focus on building a functional and responsive design for visualizing the directory tree without the learning curve or complexity that comes with advanced frontend frameworks. We can incorporate <img src="https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB"> if there is a need for dynamic features.

##### 3. Database Connection and Management:
Although <img src="https://img.shields.io/badge/Flask-000000?style=for-the-badge&logo=flask&logoColor=white"/> does not come with a built-in ORM like Django, we can use extensions like <b>Flask-SQLAlchemy</b> to integrate ORM capabilities. This enables us to still use <b>Flask's</b> lightweight framework.

##### 4. Authentication and Permission Management:
We are using OAuth for authentication (OUTDATED).

After careful consideration within our project team, we have opted to forego the integration of OAuth into our tech stack. This decision was made due to time constraints that necessitate prioritization of essential functionalities and expedited development cycles. While OAuth integration offers considerable benefits in terms of security and user authentication, the current project timeline does not afford us the opportunity to fully implement and validate its integration. As such, we have chosen to focus our resources on core project requirements to maintain project momentum.

##### 5. Development, Testing and Deployment:
We use <img src="https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white">. This aligns well with the Agile and Scrum framework we will work with as well as Flask's modular approach, allowing us to integrate various tools we need, supporting CI/CD pipelines efficiently.

##### 6. Server and Hosting Environment:
We may opt for web servers like <b>Apache</b> or <b>nginx</b> due to their robustness and <img src="https://img.shields.io/badge/Docker-2CA5E0?style=for-the-badge&logo=docker&logoColor=white"> for containerization. Docker compliments Flask's simplicity and modularity, allowing us to deploy each microservice or application component in its container.

### Standards & Conventions

<!--Here you can document your coding standards and conventions. This includes decisions about naming, style guides, etc.-->

##### Variables & Naming:

PascalCase for global variable names

camelCase for local variable & method names by default

snake_case for SQL variables

ALL-CAPS for constants

No one-letter variable names (with exception of loop control)

hyphenated-all-lower-case for HTML classes and IDs

##### Documentation:

Minimum one line commented description per method

Use [JSDoc](https://jsdoc.app/) for JavaScript

Use [DocStrings](https://peps.python.org/pep-0257/) for Python

For SQL:
- judicious white space use
- specify the primary key first right after the CREATE TABLE statement.
- all reserved words in all caps

Include descriptive messages upon commit 

##### Formatting:

Maintain proper indentation for readability (necessary for Python)

Keep open brackets inline with method header; closing bracket on its own line

Include one empty line between methods for readability 

##### Error Handling:

All functions that encounter an error condition should either return a 0 or 1 for simplifying the debugging, as well as a descriptive error message

# Frontend Tests Status

### Overview
| File | % Stmts | % Branch | % Funcs | % Lines |
| ---- | ------- | -------- | ------- | ------- |
| **All files** | 10.1 | 2.4 | 22.32 | 23.43 |
| **babel.config.js** | - | 100 | 100 | - |
| **filesystem.js** | - | - | 35 | 27.74 |
| **login.js** | 75 | 50 | 75 | 75 |
| **register.js** | 75 | 50 | 62.5 | 75 |
| **visualize.js** | 24.06 | 22.5 | 23.68 | 23.66 |


Test Suites: 4 passed, 4 total
Tests: 12 passed, 12 total
Snapshots: 0 total
Time: 1.296 s
Ran all test suites.


### Frontend Test Setup
- npm install
- npm install --save-dev jest-environment-jsdom
- install --save-dev @babel/core @babel/preset-env
- install --save-dev jest

- **DOM Simulation**: Using `jsdom`, the HTML pages are loaded into a simulated DOM environment where scripts can be executed.
- **Mocking Network Requests**: `jest-fetch-mock` is used to mock fetch API calls to test the frontend logic independently of the backend.

# Backend Tests Status

### Overview
Tests: 8 passed, 8 total;
Ran 8 tests in 0.195s;
OK


### Backend Test Setup
Rather than having the CI/CD pipeline connect to the dockerized MySQL database in a different environment than the frontend, we test the backend separately by running the following files:
1) createCopyofapp.py   (to utilize test database instead)
2) pytest.py            (to run tests)

