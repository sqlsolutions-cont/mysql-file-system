// babel.config.js
module.exports = {
    presets: [
      [
        '@babel/preset-env',
        {
          targets: {
            node: 'current',  // This tells Babel to target the current version of Node. This is useful for Jest since it will run in a Node environment.
          },
        },
      ],
    ],
  };
  