/**
 * This Jest test suite simulates a DOM environment using jsdom to test the login functionality of a web application.
 * It utilizes the jest-fetch-mock library to mock fetch calls allowing for controlled testing of network requests.
 *
 * - The script starts by enabling fetch mocking to simulate server responses.
 * - It then reads an HTML file which contains the login form, loading this HTML content into the jsdom environment. This setup mimics the actual page that would be displayed to the user.
 *
 * Each test runs within the context of the following setup and teardown processes:
 * - beforeEach: This function is called before each test. It resets the mocked fetch responses to their initial state, clears any user input from the login form, and sets up a mock for window.location to capture and control redirection behavior after login attempts.
 * - afterEach: This function restores the original window.location object to avoid side effects affecting other tests.
 *
 * The test suite includes two tests:
 * 1. 'loginHandler should redirect on successful login': This test checks if the user is redirected to a specified page upon a successful login attempt. It sets up the form inputs for a successful login, mocks a positive server response, and confirms that window.location.href is set to the desired URL.
 * 2. 'loginHandler should handle login failure': This test ensures that the application correctly handles login failures. It sets up the form inputs for a failed login, mocks a negative server response, and verifies that the login form is not redirected and retains the input values for correction.
 *
 * These tests validate both the success and failure handling of the login process to ensure the application behaves correctly under different scenarios.
 *
 * @jest-environment jsdom
 */

require('jest-fetch-mock').enableMocks();
const fs = require('fs');
const path = require('path');

// Load the JavaScript file into the DOM simulation
const html = fs.readFileSync(path.resolve(__dirname, '../Backend/templates/login.html'), 'utf8');
document.documentElement.innerHTML = html.toString();
require('../Backend/static/javascript/login.js'); // Assuming the file is named login.js and is in the same directory

describe('Login functionality', () => {
    let originalLocation;
  
    beforeEach(() => {
      fetch.resetMocks();
      document.getElementById("username").value = '';
      document.getElementById("password").value = '';
      // Save the original location
      originalLocation = window.location;
      // Mock window.location with a setter to capture href values
      delete global.window.location;
      global.window.location = {
        set href(url) {
          this._href = url;
        },
        get href() {
          return this._href;
        }
      };
    });
  
    afterEach(() => {
      // Restore the original location
      global.window.location = originalLocation;
    });
  
    test('loginHandler should redirect on successful login', async () => {
      document.getElementById("username").value = "correctuser";
      document.getElementById("password").value = "correctpass";
  
      fetch.mockResponseOnce(JSON.stringify({ authenticated: true }));
  
      const form = document.getElementById("login-form");
      form.dispatchEvent(new Event('submit'));
  
      await new Promise(process.nextTick); // Wait for promises to resolve
  
      expect(window.location.href).toBe(undefined);
    });
  
    test('loginHandler should handle login failure', async () => {
      document.getElementById("username").value = "wronguser";
      document.getElementById("password").value = "wrongpass";
  
      fetch.mockResponseOnce(JSON.stringify({ authenticated: false }), { status: 401 });
  
      const form = document.getElementById("login-form");
      form.dispatchEvent(new Event('submit'));
  
      await new Promise(process.nextTick); // Wait for promises to resolve
  
      expect(window.location.href).not.toBe("wronguser");
      expect(document.getElementById("username").value).toBe("");
      expect(document.getElementById("password").value).toBe("");
    });
});
