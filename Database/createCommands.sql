/* These are the SQL equivalents of the databases we have created in Flask */

/*
This table holds each unique file and directory in our file system.
*/
CREATE TABLE Files (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    data VARCHAR(16000),
    created DATETIME NOT NULL,
    updated DATETIME NOT NULL,
    parent INTEGER NOT NULL,
    is_file BOOLEAN NOT NULL
);

/*
This table holds the total number of user-file permission pairs in our file system.
*/
CREATE TABLE Permissions (
    perm_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    user_id INTEGER NOT NULL,
    file_id INTEGER NOT NULL,
    r_perm BOOLEAN NOT NULL,
    w_perm BOOLEAN NOT NULL,
    a_perm BOOLEAN NOT NULL,
    FOREIGN KEY (file_id) REFERENCES Files(id),
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

/*
This table holds each unique user that can access our file system.
*/
CREATE TABLE Users (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL
);
