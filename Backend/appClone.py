"""
This module forms the bulk of our file system backend, receiving input from the frontend and 
querying the MySQL database appropriately based on the user's request.
"""

from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from flask_cors import CORS
from typing import Dict, Tuple, Any

# instantiate Flask app
app = Flask(__name__)
CORS(app)
# connect Flask app to our MySQL database running in docker container on CSL machine
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:asdf@localhost/TestDB'

db = SQLAlchemy(app)

# Define model classes
with app.app_context():
    db.reflect()

class Files(db.Model):
    """
    Model representing the Files table in MySQL database

    Public Methods:
        query(): Returns a query object for the model.
        filter: Adds filtering conditions to a query.
        all: Returns all instances of the model.
        get: Retrieves a single instance by primary key.
        first: Returns the first result of a query.
        save(): Adds or updates the current instance to the database session.
        delete(): Deletes the current instance from the database session.
        to_dict(): Converts the model instance to a dictionary.
    
    Public Instance Variables:
        __tablename__ (str): The name of the database table associated with the model.
        id (int): primary key identifying the unique file
        name (str): name of the file
        data (str): data of the file. blank if is a directory
        created (datetime): the date this file was created
        updated (datetime): the date this file was last updated
        parent (int): the file id of this file's parent directory
        is_file (boolean): true if this entry is a file. false if this entry is a directory
    """

    __table__ = db.metadata.tables["Files"]

class Permissions(db.Model):
    """
    Model representing the Permissions table in MySQL database
    
    Public Methods:
        query(): Returns a query object for the model.
        filter: Adds filtering conditions to a query.
        all: Returns all instances of the model.
        get: Retrieves a single instance by primary key.
        first: Returns the first result of a query.
        save(): Adds or updates the current instance to the database session.
        delete(): Deletes the current instance from the database session.
        to_dict(): Converts the model instance to a dictionary.
    
    Public Instance Variables:
        __tablename__ (str): The name of the database table associated with the model.
        perm_id (int): primary key identifying the unique file-user pair
        user_id (int): id of the user. references Users.id
        file_id (int): id of the file. references Files.id
        r_perm (boolean): true if the user has read permissions for the file. false otherwise
        w_perm (boolean): true if the user has write permissions for the file. false otherwise
        a_perm (boolean): true if the user has admin permissions for the file. false otherwise
            - allows a user to change file's read and write permissions for other users
    """

    __table__ = db.metadata.tables["Permissions"]

class Users(db.Model):
    """
    Model representing the Users table in MySQL database
    
    Public Methods:
        query(): Returns a query object for the model.
        filter: Adds filtering conditions to a query.
        all: Returns all instances of the model.
        get: Retrieves a single instance by primary key.
        first: Returns the first result of a query.
        save(): Adds or updates the current instance to the database session.
        delete(): Deletes the current instance from the database session.
        to_dict(): Converts the model instance to a dictionary.
    
    Public Instance Variables:
        id (int): primary key identifying the unique user
        username (str): username of the user
        password (str): password of the user
    """
    __table__ = db.metadata.tables["Users"]

# set userID to Admin's by default
userID = 7
# set curDir (current Directory) to root node id of 1
curDir = 1

@app.route('/directory', methods=['POST'])
def update_directory() -> Tuple[str, int]:
    """
    Flask route to update the user's current directory.
    
    Returns:
        Tuple[str, int]: A tuple containing a JSONified message of updating directory and a status code.
    """
    # Update directory logic goes here
    global curDir
    frontendRequest = request.json
    curDir = frontendRequest['currentDirectory']
    return jsonify({'message': 'Directory Updated'}), 200


@app.route('/users', methods=['GET'])
def get_users() -> str:
    """
    Flask route to get all users
    
    Returns:
        str: A JSONified message of getting all users
    """
    users = Users.query.all()
    output = []
    for user in users:
        user_data = {}
        user_data['id'] = user.id
        user_data['username'] = user.username
        user_data['password'] = user.password


        # temporary permission bypass
        output.append(user_data)

        # readPerm = db.session.query(Permissions.r_perm).filter(file_id = file.id, user_id = userID)
        # if readPerm == 1:
        #     output.append(file_data)
    # print({'files': output})
    return jsonify({'users': output})

@app.route("/users", methods=['POST'])
def createUser() -> str:
    """
    Flask route to create new user
    
    Returns:
        str: A JSONified message of creating a new user
    """

    # access data submitted with HTTP request
    userInfo = request.json

    # create file object with request data
    newUser = Users(
        username=userInfo['username'], 
        password=userInfo['password']
    )

    # check that user has write permissions to the parent directory using the following SQL query:
    # SELECT w_perm 
    # FROM Permissions 
    # WHERE Permissions.user_id = myUserID 
    # AND Permissions.file_id = fileID

    # id below is supposed to reference primary key of Files model table. 

    ###Create some dummy permission data so that this works
    # writePermission = db.session.execute(db.select(w_perm).where(user_id == myUserID and file_id == id)).scalar()
    # if not writePermission:
    #     return jsonify({'message': 'You do not have write permissions for this directory'})

    # add new file object to database
    db.session.add(newUser)
    db.session.flush()  # This will assign an ID to newUser without committing the transaction

    # Retrieve all existing files from the database
    all_files = Files.query.all()
    for file in all_files:
        # Create a permission entry for the new user for each file with all permissions set to 0
        permission = Permissions(user_id=newUser.id, file_id=file.id, r_perm=0, w_perm=0, a_perm=0)
        db.session.add(permission)

    # Commit the transaction to save the user and all permissions to the database
    db.session.commit()

    return jsonify({'message': 'User created', 'user_id': newUser.id})

@app.route("/getSingleFile", methods=['POST'])
def getSingleFile() -> Tuple[str, int]:
    """
    Flask route to get single file with matching name in cur directory from database
    
    Returns:
        Tuple[str, int]: A tuple containing a JSONified message of acquiring a single file and a status code.
    """
    
    # access data submitted with HTTP request
    frontendRequest = request.json
    frontendFileName = frontendRequest['fileName']

    # access requested file name from database
    fileRecord = Files.query.filter_by(name = frontendFileName).first()
    if not fileRecord:
        return jsonify({'message': 'No file found'}), 404
    
    fileName = fileRecord.name
    fileData = fileRecord.data
    fileCreated = fileRecord.created
    fileUpdated = fileRecord.updated

    return jsonify({'name': fileName, 'data': fileData, 'created': fileCreated, 'updated': fileUpdated}), 200

# describe format of expected input
@app.route('/files', methods=['GET'])
def get_files() -> str:
    """
    Flask route to get all files in database

    Returns:
        str: A JSONified message of getting all files
    """
    files = Files.query.all()
    output = []
    for file in files:
        file_data = {}
        file_data['id'] = file.id
        file_data['name'] = file.name
        file_data['data'] = file.data
        # fix datetime format to json
        file_data['created'] = file.created.strftime('%Y-%m-%d %H:%M:%S')
        file_data['updated'] = file.updated.strftime('%Y-%m-%d %H:%M:%S')
        file_data['parent'] = file.parent
        file_data['is_file'] = file.is_file

        # temporary permission bypass
        output.append(file_data)

        # readPerm = db.session.query(Permissions.r_perm).filter(file_id = file.id, user_id = userID)
        # if readPerm == 1:
        #     output.append(file_data)
    # print({'files': output})
    return jsonify({'files': output})

@app.route('/permissions', methods=['GET'])
def get_permissions() -> str:
    """
    Flask route to get all permissions in database
    
    Returns:
        str: A JSONified message of returning permission output.
    """
    permissions = Permissions.query.all()
    output = []
    for permission in permissions:
        perm_data = {}
        perm_data['perm_id'] = permission.perm_id
        perm_data['user_id'] = permission.user_id
        perm_data['file_id'] = permission.file_id
        perm_data['r_perm'] = permission.r_perm
        perm_data['w_perm'] = permission.w_perm
        perm_data['a_perm'] = permission.a_perm
        output.append(perm_data)
    return jsonify({'permissions': output})


@app.route("/files", methods=['POST'])
def createFile() -> str:
    """
    Flask route to create new file or directory
    
    Returns:
        str: A JSONified message of creating file.
    """
    
    # access data submitted with HTTP request
    fileInfo = request.json

    # create file object with request data
    newFile = Files(
        name=fileInfo['name'], 
        data=fileInfo['data'], 
        created=fileInfo['created'], 
        updated=fileInfo['updated'], 
        parent=curDir,
        is_file=fileInfo['is_file'])

    parentID = curDir
    frontendUsername = fileInfo['userToPass']
    userRecord = Users.query.filter_by(username = frontendUsername).first()
    userID = userRecord.id
    permRecord = Permissions.query.filter_by(file_id = parentID, user_id = userID).first()
    writePerm = permRecord.w_perm
    if writePerm != 1:
        return jsonify({'message': 'You do not have permission to do that.'}), 401

    # add new file object to database
    db.session.add(newFile)
    db.session.flush()  # This will assign an ID to newFile without committing the transaction

    # Assuming the user has write permission on the parent, let's add the file and permissions
    all_users = Users.query.all()
    for user in all_users:
        permission = Permissions(user_id=user.id, file_id=newFile.id, r_perm=1, w_perm=1, a_perm=1)
        db.session.add(permission)

    db.session.commit()  # This commits both the new file and all the new permissions
    db.session.commit()
    return jsonify({'message': 'File created', 'file_id': newFile.id})

@app.route('/permissions', methods=['POST'])
def create_permissions() -> Tuple[str, int]:
    """
    Flask route to add new file-user permission pair
    
    Returns:
        Tuple[str, int]: A tuple containing a JSONified message of created permissions and a status code.
    """
    # Assume the request will contain a list of permissions
    permissions_info = request.json  # This should be a list of dictionaries

    for perm_info in permissions_info:
        new_permission = Permissions(
            user_id=perm_info['user_id'],
            file_id=perm_info['file_id'],
            r_perm=perm_info['r_perm'],
            w_perm=perm_info['w_perm'],
            a_perm=perm_info['a_perm']
        )
        db.session.add(new_permission)

    # Commit all new permissions at once
    db.session.commit()

    return jsonify({'message': f'{len(permissions_info)} permissions created'}), 201

@app.route('/user_permissions', methods=['GET'])
def user_file_permissions() -> Tuple[str, int]:
    """
    Flask route to get dictionary mapping all user keys to list of files they have read access for

    Returns:
        Tuple[str, int]: A tuple containing a JSONified message of user permissions and a status code.
    """
    # Query to fetch users with read permissions
    permissions = db.session.query(
        Permissions.user_id, Files.name
    ).join(Files, Permissions.file_id == Files.id).filter(Permissions.r_perm == True).all()

    output = {}
    for user_id, file_name in permissions:
        if user_id in output:
            output[user_id].append(file_name)
        else:
            output[user_id] = [file_name]

    return jsonify(output)
    
@app.route('/renameFile', methods=['PUT'])
def rename_file() -> Tuple[str, int]:
    """
    Flask route to rename a file or directory.
    
    Returns:
        Tuple[str, int]: A tuple containing a JSONified message of renaming file result and a status code.
    """
    # Update logic goes here
    frontendRequest = request.json
    frontendOldName = frontendRequest['oldName']
    frontendNewName = frontendRequest['newName']
    frontendUpdated = frontendRequest['updated']
    frontendUsername = frontendRequest['userToPass']
    
    fileRecord = Files.query.filter_by(parent = curDir).filter_by(name = frontendOldName).first()
    if not fileRecord:
        return jsonify({'message': 'No file found'}), 404
    fileID = fileRecord.id

    userRecord = Users.query.filter_by(username = frontendUsername).first()
    userID = userRecord.id
    permRecord = Permissions.query.filter_by(file_id = fileID, user_id = userID).first()
    writePerm = permRecord.w_perm
    if writePerm == 1:
        fileRecord.name = frontendNewName
        fileRecord.updated = frontendUpdated
        db.session.commit()
        return jsonify({'message': 'File renamed'}), 200
    else:
        return jsonify({'message': 'You do not have permission to do that.'}), 401
    
@app.route('/updateFile', methods=['PUT'])
def update_file() -> Tuple[str, int]:
    """
    Flask route to update a file.
    
    Returns:
        Tuple[str, int]: A tuple containing a JSONified message of updating file result and a status code.
    """
    # Update logic goes here
    frontendRequest = request.json
    frontendName = frontendRequest['name']
    frontendData = frontendRequest['data']
    frontendUpdated = frontendRequest['updated']
    frontendUsername = frontendRequest['userToPass']
    
    fileRecord = Files.query.filter_by(parent = curDir).filter_by(name = frontendName).first()
    if not fileRecord:
        return jsonify({'message': 'No file found'}), 404
    if fileRecord.is_file == 0:
        return jsonify({'message': 'Updated file cannot be a directory'}), 404
    fileID = fileRecord.id

    userRecord = Users.query.filter_by(username = frontendUsername).first()
    userID = userRecord.id
    permRecord = Permissions.query.filter_by(file_id = fileID, user_id = userID).first()
    writePerm = permRecord.w_perm
    if writePerm == 1:
        fileRecord.name = frontendName
        fileRecord.data = frontendData
        fileRecord.updated = frontendUpdated
        db.session.commit()
        return jsonify({'message': 'File updated'}), 200
    else:
        return jsonify({'message': 'You do not have permission to do that.'}), 401

@app.route('/moveFile', methods=['PUT'])
def move_file() -> Tuple[str, int]:
    """
    Flask route to move a file or directory.
    
    Returns:
        Tuple[str, int]: A tuple containing a JSONified message of moving file/directory result and a status code.
    """
    # Move logic goes here
    frontendRequest = request.json
    frontendName = frontendRequest['name']
    frontendDestination = frontendRequest['destination']
    frontendUsername = frontendRequest['userToPass']

    fileRecord = Files.query.filter_by(parent = curDir).filter_by(name = frontendName).first()
    if not fileRecord:
        return jsonify({'message': 'No file found'}), 404

    # if condition if moving up a directory or moving into a directory in current directory
    if frontendDestination == "..":
        parentRecord = Files.query.filter_by(id = fileRecord.parent).first()
        destinationRecord = Files.query.filter_by(id = parentRecord.parent).first()
    else:
        destinationRecord = Files.query.filter_by(name = frontendDestination).first()
    
    if not destinationRecord:
        return jsonify({'message': 'No destination found'}), 404
    if destinationRecord.is_file == 1:
        return jsonify({'message': 'Destination must be a directory'}), 404
    
    destinationID = destinationRecord.id
    
    # check permissions
    userRecord = Users.query.filter_by(username = frontendUsername).first()
    userID = userRecord.id
    permRecord = Permissions.query.filter_by(file_id = destinationID, user_id = userID).first()
    writePerm = permRecord.w_perm
    if writePerm == 1:
        # execute the move
        fileRecord.parent = destinationID
        db.session.commit()
        return jsonify({'message': 'File updated'}), 200
    else:
        return jsonify({'message': 'You do not have permission to do that.'}), 401

@app.route('/files/<string:deleteFileName>', methods=['DELETE'])
def delete_file(deleteFileName) -> Tuple[str, int]:
    """
    Flask route to delete a file

    Returns:
        Tuple[str, int]: A tuple containing a JSONified message of deleting file and a status code.
    """
    
    # Fetch file to be deleted if it exists
    frontendRequest = request.json
    fileName = frontendRequest['name']
    fileRecord = Files.query.filter_by(parent = curDir).filter_by(name = fileName).first()
    if not fileRecord:
        return jsonify({'message': 'No file found'}), 404
    
    # Permission check
    frontendUsername = frontendRequest['userToPass']
    
    fileID = fileRecord.id
    userRecord = Users.query.filter_by(username = frontendUsername).first()
    userID = userRecord.id
    permRecord = Permissions.query.filter_by(file_id = fileID, user_id = userID).first()
    writePerm = permRecord.w_perm
    if writePerm != 1:
        return jsonify({'message': 'You do not have permission to do that.'}), 401
    
    # Remove all permissions associated with the file to be deleted
    Permissions.query.filter_by(file_id=fileRecord.id).delete()

    db.session.delete(fileRecord)
    db.session.commit()
    return jsonify({'message': 'File deleted'}), 200

@app.route('/users/<int:user_id>', methods=['DELETE'])
def delete_user(user_id) -> Tuple[str, int]:
    """
    Flask route to delete a user

    Returns:
        Tuple[str, int]: A tuple containing a JSONified message of deleting user and perms and a status code.
    """
    # Query for the user with the given ID
    user = Users.query.get(user_id)

    # If the user does not exist, return a 404 error
    if not user:
        return jsonify({'message': 'No user found'}), 404

    # First delete all permissions associated with the user
    Permissions.query.filter_by(user_id=user_id).delete()

    # Then delete the user itself
    db.session.delete(user)

    # Commit the changes to the database
    db.session.commit()

    # Return a success message
    return jsonify({'message': 'User and associated permissions deleted'}), 200

@app.route('/permissions', methods=['PUT'])
def update_permissions() -> Tuple[str, int]:
    """
    Flask route to update permissions

    Returns:
        Tuple[str, int]: A tuple containing a JSONified message of updating permissions and perms and a status code.
    """
    # You may want to include authentication and authorization checks here
    # to ensure that the requester has the authority to update permissions.

    # Parse the incoming JSON data for permission updates
    permission_data = request.json
    try:
        for perm in permission_data['permissions']:
            user_id = perm['user_id']
            file_id = perm['file_id']
            r_perm = perm.get('r_perm')
            w_perm = perm.get('w_perm')
            a_perm = perm.get('a_perm')

            # Retrieve the permission object from the database
            permission = Permissions.query.filter_by(user_id=user_id, file_id=file_id).first()
            if not permission:
                # If the permission doesn't exist, we can't update it
                return jsonify({'message': 'Permissions not found'}), 404

            # Update the permissions based on the provided data
            if r_perm is not None:
                permission.r_perm = r_perm
            if w_perm is not None:
                permission.w_perm = w_perm
            if a_perm is not None:
                permission.a_perm = a_perm

            # Save the updated permission object to the database
            db.session.commit()
    except KeyError:
        # If we are missing data in the request, return an error
        return jsonify({'message': 'Missing data in request'}), 400

    # Return a success message
    return jsonify({'message': 'Permissions updated'}), 200

@app.route('/login', methods=['POST'])
def login() -> Tuple[str, int]:
    """
    Flask route to login

    Returns:
        Tuple[str, int]: A tuple containing a JSONified message of logging in and a status code.
    """
    # Parse the incoming JSON data for username and password
    login_data = request.json
    username = login_data.get('username')
    password = login_data.get('password')

    if not username or not password:
        # If username or password are not provided, return an error
        return jsonify({'message': 'Username and password are required'}), 400

    # Retrieve the user from the database
    user = Users.query.filter_by(username=username).first()

    if user and user.password == password:
        # If the user exists and the password matches
        return jsonify({'message': 'Login successful'}), 200
    else:
        # If the user doesn't exist or password does not match
        return jsonify({'message': 'Invalid username or password'}), 401

@app.route('/registerUser', methods=['POST'])
def registerUser() -> Tuple[str, int]:
    """
    Flask route to register

    Returns:
        Tuple[str, int]: A tuple containing a JSONified message of registering and a status code.
    """
    # Parse the incoming JSON data for username and password
    register_data = request.json
    username = register_data.get('username')
    password = register_data.get('password')

    if not username or not password:
        # If username or password are not provided, return an error
        return jsonify({'message': 'Username and password are required'}), 400

    # Retrieve the user from the database
    user = Users.query.filter_by(username=username).first()

    if user:
        # If the user already exists
        return jsonify({'message': 'User already exists'}), 400
    else:
        # If the user doesn't exist, create a new user
        new_user = Users(username=username, password=password)
        db.session.add(new_user)
        db.session.commit()
        return jsonify({'message': 'User registered'}), 201

@app.route('/register.html', methods=['POST', 'GET'])
def register() -> str:
    """
    Flask route for registering frontend pages.
    
    Returns:
        str: The rendered HTML content of the 'register.html' template.
    """
    return render_template('register.html')

@app.route('/filesystem.html', methods=['POST', 'GET'])
def filesystem() -> str:
    """
    Flask route for filesystem frontend pages.
    
    Returns:
        str: The rendered HTML content of the 'filesystem.html' template.
    """
    return render_template('filesystem.html')

@app.route('/', methods=['POST', 'GET'])
def index() -> str:
    """
    Flask route for the index page.
    
    Returns:
        str: The rendered HTML content of the 'login.html' template.
    """
    return render_template('login.html')

def run_flask_app() -> None:
    """
    Run the Flask app on local port 8080.
    """
    app.run(host="localhost", port=8080, debug=True)

if __name__ == "__main__":
    run_flask_app()
