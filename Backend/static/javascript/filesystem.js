/**
 * @file Contains functions for handling button clicks and form submissions.
 */
import { passCurrentDirectory, visualizeHandler } from "./visualize.js";

// get stored username
const user = localStorage.getItem("username");
let isAdmin = false;
if (!user) { // redirect to login page if no username stored
    console.error("not logged in");
    window.location.href = "login.html";
} else {
    console.log("logged in as", user);
    // if user is Admin (really not secure)
    isAdmin = (user === "Admin");
    console.log("is Admin", isAdmin);
}

/**
 * Adds event listeners to buttons when window loads.
 * @listens window.onload
 * @see passCurrentDirectory
 * @see visualizeHandler
 * 
 * @see draggablePopups
 * @see logoutHandler
 * @see closePopupsHandler
 * @see newFileHandler
 * @see newDirectoryHandler
 * @see moveFileHandler
 * @see renameFileHandler
 * @see updateFileHandler
 * @see deleteFileHandler
 * @see loadPermissionsHandler
 * @see editPermissionsHandler
 */
window.onload = function onLoadHandler() {
    // only show edit permissions button when logged in as Admin
    if (isAdmin) {
        document.getElementsByClassName("nav-bar")[0].style.justifyContent = "space-between";
        document.getElementById("edit-perms-button").style.display = "block";
    }
    passCurrentDirectory();
    visualizeHandler();

    // makes all popups draggable
    let popups = document.querySelectorAll(".popup");
    [].forEach.call(popups, function (popup) {
        draggablePopups(popup);
    });

    // logs out when clicking logout button
    document.getElementById("logout-button").addEventListener("click", logoutHandler);

    // closes all forms when clicking any menu button
    var menuButtons = document.querySelectorAll(".nav-bar .button");
    [].forEach.call(menuButtons, function (menuButton) {
        menuButton.addEventListener("click", closePopupsHandler);
    });

    // closes all forms when clicking x
    var closeButtons = document.getElementsByClassName("close-button");
    [].forEach.call(closeButtons, function (closeButton) {
        closeButton.addEventListener("click", closePopupsHandler);
    });

    // makes new file form visible when clicking "New File" button
    document.getElementById("new-file-button").addEventListener("click", () => {
        document.getElementById("new-file-popup").classList.add("show");
    });

    // calls newFileHandler when new file form is submitted
    document.getElementById("new-file-form").addEventListener("submit", newFileHandler);

    // makes new directory form visible when clicking "New Directory" button
    document.getElementById("new-dir-button").addEventListener("click", () => {
        document.getElementById("new-dir-popup").classList.add("show");
    });

    // calls newDirectoryHandler when new directory form is submitted
    document.getElementById("new-dir-form").addEventListener("submit", newDirectoryHandler);

    // makes move file form visible when clicking "Move File/Directory" button
    document.getElementById("move-file-button").addEventListener("click", () => {
        document.getElementById("move-file-popup").classList.add("show");
    });

    // calls moveFileHandler when move file form is submitted
    document.getElementById("move-file-form").addEventListener("submit", moveFileHandler);

    // makes rename file form visible when clicking "Rename File" button
    document.getElementById("rename-file-button").addEventListener("click", () => {
        document.getElementById("rename-file-popup").classList.add("show");
    });

    // calls renameFileHandler when rename file form is submitted
    document.getElementById("rename-file-form").addEventListener("submit", renameFileHandler);

    // makes update file form visible when clicking "Update File" button
    document.getElementById("update-file-button").addEventListener("click", () => {
        document.getElementById("update-file-popup").classList.add("show");
    });

    // calls updateFileHandler when update file form is submitted
    document.getElementById("update-file-form").addEventListener("submit", updateFileHandler);

    // makes delete file form visible when clicking "Delete File" button
    document.getElementById("delete-file-button").addEventListener("click", () => {
        document.getElementById("delete-file-popup").classList.add("show");
    });

    // calls deleteFileHandler when delete file form is submitted
    document.getElementById("delete-file-form").addEventListener("submit", deleteFileHandler);

    // makes edit permissions form visible when clicking "Edit Permissions" button
    document.getElementById("edit-perms-button").addEventListener("click", () => {
        document.getElementById("edit-perms-popup").classList.add("show");
        document.getElementById("user-dropdown").innerHTML = "<option value=\"default\" selected disabled hidden>Select User</option>";
        loadPermissionsHandler();
    });

    // calls editPermissionsHandler when edit permissions form is submitted
    document.getElementById("edit-perms-form").addEventListener("submit", editPermissionsHandler);
};

/**
 * Makes a popup draggable.
 * @param {HTMLElement} popup The popup to make draggable.
 */
function draggablePopups(popup) {
    var offsetX, offsetY;
    var popupWidth = popup.offsetWidth;
    var popupHeight = popup.offsetHeight;

    // controls what/when to start dragging
    function dragDown(e) {
        if ((["INPUT", "TEXTAREA", "SELECT"].includes(e.target.tagName))) {
            return;
        }
        offsetX = e.clientX - popup.offsetLeft;
        offsetY = e.clientY - popup.offsetTop;
        document.addEventListener("mousemove", dragMove);
        document.addEventListener("mouseup", dragUp);
    }

    // moves popup based on mouse movement
    function dragMove(e) {
        var newX = e.clientX - offsetX;
        var newY = e.clientY - offsetY;

        if (newX >= 0 && newX <= window.innerWidth - popupWidth) {
            popup.style.left = newX + "px";
        }

        if (newY >= 0 && newY <= window.innerHeight - popupHeight) {
            popup.style.top = newY + "px";
        }
    }

    // stops dragging when mouse is released
    function dragUp() {
        document.removeEventListener("mousemove", dragMove);
        document.removeEventListener("mouseup", dragUp);
    }

    popup.addEventListener("mousedown", dragDown);
}

/**
 * Handles logging out.
 * Removes username from localStorage, redirects to login page.
 */
function logoutHandler() {
    console.log("logging out", localStorage.getItem("username"));
    localStorage.removeItem("username");
    window.location.href = "login.html";
}

/**
 * Closes all popups.
 * @param {Event} event The x button click event.
 */
function closePopupsHandler(event) {
    event.preventDefault(); // prevents page from reloading

    // reset all forms
    const forms = document.querySelectorAll(".popup form");
    forms.forEach(form => form.reset());

    // clear permission table, disable update button
    document.getElementById("perms-table").style.display = "none";
    document.querySelector("#edit-perms-form button").setAttribute("disabled", "");

    // hide all class="popup" elements
    var popups = document.getElementsByClassName("popup show");
    [].forEach.call(popups, function (popup) {
        if (popup.id !== "file-display") {
            popup.classList.remove("show");
            console.log(popup.id, "hidden");
        }
    });
}

/**
 * Handles data from the new file form and resets the form.
 * @param {Event} event The new file form submission event.
 * @see newFileBackendHandler
 */
function newFileHandler(event) {
    event.preventDefault(); // prevents page from reloading

    const name = document.getElementById("new-file-name").value;
    const data = document.getElementById("new-file-data").value;
    console.log(name, ":", data);

    // get current date and time
    const currentDate = new Date();
    // adjust for timezone offset
    currentDate.setMinutes(currentDate.getMinutes() - currentDate.getTimezoneOffset());
    // format date and time to be compatible with MySQL database
    const currentDT = currentDate.toISOString().slice(0, 19).replace("T", " ") + ".000000";
    console.log(currentDT);

    newFileBackendHandler(name, data, currentDT, currentDT);

    // hide and reset form
    document.getElementById("new-file-popup").classList.remove("show");
    document.getElementById("new-file-name").value = "";
    document.getElementById("new-file-data").value = "";

    visualizeHandler();
}

/**
 * Handles creating a new file on the backend.
 * @param {string} name The name of the file.
 * @param {string} data The contents of the file.
 * @param {string} created The creation timestamp of the file.
 * @param {string} updated The last updated timestamp of the file.
 * @see newFileHandler
 */
function newFileBackendHandler(name, data, created, updated) {
    let userToPass = user // username to hand to backend
    let is_file = "1"
    const jsonBody = JSON.stringify({ name, data, created, updated, is_file, userToPass });
    console.log(jsonBody);

    console.log("new file created");

    fetch("http://127.0.0.1:5000/files", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: jsonBody
    })
        .then((r) => {
            if (!r.ok) {
                throw (new Error(r));
            }
            return (r.json());
        })
        .then((d) => {
            console.log(d);
        })
        .catch((e) => {
            console.error(e);
        });
}

/**
 * Handles data from the new directory form and resets the form.
 * @param {Event} event The new directory form submission event.
 * @see newDirectoryBackendHandler
 */
function newDirectoryHandler(event) {
    event.preventDefault(); // prevents page from reloading

    const name = document.getElementById("new-dir-name").value;
    console.log(name);

    // get current date and time
    const currentDate = new Date();
    // adjust for timezone offset
    currentDate.setMinutes(currentDate.getMinutes() - currentDate.getTimezoneOffset());
    // format date and time to be compatible with MySQL database
    const currentDT = currentDate.toISOString().slice(0, 19).replace("T", " ") + ".000000";
    console.log(currentDT);

    newDirectoryBackendHandler(name, currentDT, currentDT);

    // hide and reset form
    document.getElementById("new-dir-popup").classList.remove("show");
    document.getElementById("new-dir-name").value = "";

    visualizeHandler();
}

/**
 * Handles creating a new directory on the backend.
 * @param {string} name The name of the directory.
 * @param {string} created The creation timestamp of the directory.
 * @param {string} updated The last updated timestamp of the directory.
 * @see newDirectoryHandler
 */
function newDirectoryBackendHandler(name, created, updated) {
    let data = ""; // empty since no data for directory
    let userToPass = user // username to hand to backend
    let is_file = "0";
    const jsonBody = JSON.stringify({ name, data, created, updated, is_file, userToPass });
    console.log(jsonBody);

    console.log("new directory created");

    fetch("http://127.0.0.1:5000/files", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: jsonBody
    })
        .then((r) => {
            if (!r.ok) {
                throw (new Error(r));
            }
            return (r.json());
        })
        .then((d) => {
            console.log(d);
        })
        .catch((e) => {
            console.error(e);
        });
}

/**
 * Handles data from the file move form and resets the form.
 * @param {Event} event The move file form submission event.
 * @see moveFileBackendHandler
 */
function moveFileHandler(event) {
    event.preventDefault(); // prevents page from reloading

    const name = document.getElementById("move-file-name").value;
    const destination = document.getElementById("move-file-destination").value;
    console.log(name, destination);

    moveFileBackendHandler(name, destination);

    // hide and reset form
    document.getElementById("move-file-popup").classList.remove("show");
    document.getElementById("move-file-name").value = "";
    document.getElementById("move-file-destination").value = "";

    visualizeHandler();
}

/**
 * Handles moving a file on the backend.
 * @param {string} name The name of the file.
 * @param {string} destination The destination directory of the file.
 * @see moveFileHandler
 */
function moveFileBackendHandler(name, destination) {
    let userToPass = user // username to hand to backend
    const jsonBody = JSON.stringify({ name, destination, userToPass });
    console.log(jsonBody);

    console.log("file moved");

    fetch("http://127.0.0.1:5000/moveFile", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: jsonBody
    })
        .then((r) => {
            if (!r.ok) {
                throw (new Error(r));
            }
            return (r.json());
        })
        .then((d) => {
            console.log(d);
        })
        .catch((e) => {
            console.error(e);
        });
}

/**
 * Handles data from the file rename form and resets the form.
 * @param {Event} event The rename file form submission event.
 * @see renameFileBackendHandler
 */
function renameFileHandler(event) {
    event.preventDefault(); // prevents page from reloading

    const oldName = document.getElementById("old-file-rename").value;
    const newName = document.getElementById("new-file-rename").value;
    console.log(oldName, newName);

    // get current date and time
    const currentDate = new Date();
    // adjust for timezone offset
    currentDate.setMinutes(currentDate.getMinutes() - currentDate.getTimezoneOffset());
    // format date and time to be compatible with MySQL database
    const currentDT = currentDate.toISOString().slice(0, 19).replace("T", " ") + ".000000";
    console.log(currentDT);

    renameFileBackendHandler(oldName, newName, currentDT);

    // hide and reset form
    document.getElementById("rename-file-popup").classList.remove("show");
    document.getElementById("old-file-rename").value = "";
    document.getElementById("new-file-rename").value = "";

    visualizeHandler();
}

/**
 * Handles renaming a file on the backend.
 * Searches by old name, updates to new name.
 * @param {string} oldName The old name of the file.
 * @param {string} newName The new name of the file.
 * @param {string} updated The last updated timestamp of the file.
 * @see renameFileHandler
 */
function renameFileBackendHandler(oldName, newName, updated) {
    let userToPass = user; // username to hand to backend
    const jsonBody = JSON.stringify({ oldName, newName, updated, userToPass });
    console.log(jsonBody);

    console.log("file renamed");

    fetch("http://127.0.0.1:5000/renameFile", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: jsonBody
    })
        .then((r) => {
            if (!r.ok) {
                throw (new Error(r));
            }
            return (r.json());
        })
        .then((d) => {
            console.log(d);
        })
        .catch((e) => {
            console.error(e);
        });
}

/**
 * Handles data from the file update form and resets the form.
 * @param {Event} event The update file form submission event.
 * @see updateFileBackendHandler
 */
function updateFileHandler(event) {
    event.preventDefault(); // prevents page from reloading

    const name = document.getElementById("update-file-name").value;
    const data = document.getElementById("update-data").value;
    console.log(name, data);

    // get current date and time
    const currentDate = new Date();
    // adjust for timezone offset
    currentDate.setMinutes(currentDate.getMinutes() - currentDate.getTimezoneOffset());
    // format date and time to be compatible with MySQL database
    const currentDT = currentDate.toISOString().slice(0, 19).replace("T", " ") + ".000000";
    console.log(currentDT);

    updateFileBackendHandler(name, data, currentDT);

    // hide and reset form
    document.getElementById("update-file-popup").classList.remove("show");
    document.getElementById("update-file-name").value = "";
    document.getElementById("update-data").value = "";

    visualizeHandler();
}

/**
 * Handles updating a file on the backend.
 * Searches by name, updates contents.
 * @param {string} name The name of the file.
 * @param {string} data The new contents of the file.
 * @param {string} updated The last updated timestamp of the file.
 * @see updateFileHandler
 */
function updateFileBackendHandler(name, data, updated) {
    let userToPass = user // username to hand to backend
    const jsonBody = JSON.stringify({ name, data, updated, userToPass });
    console.log(jsonBody);

    console.log("file updated");

    fetch("http://127.0.0.1:5000/updateFile", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: jsonBody
    })
        .then((r) => {
            if (!r.ok) {
                throw (new Error(r));
            }
            return (r.json());
        })
        .then((d) => {
            console.log(d);
        })
        .catch((e) => {
            console.error(e);
        });
}

/**
 * Handles data from the file delete form and resets the form.
 * @param {Event} event The delete file form submission event.
 * @see deleteFileBackendHandler
 */
function deleteFileHandler(event) {
    event.preventDefault(); // prevents page from reloading

    const name = document.getElementById("delete-file-name").value;
    console.log(name);

    deleteFileBackendHandler(name);

    // hide and reset form
    document.getElementById("delete-file-popup").classList.remove("show");
    document.getElementById("delete-file-name").value = "";

    visualizeHandler();
}

/**
 * Handles deleting a file on the backend.
 * Searches by name.
 * @param {string} name The name of the file.
 * @see deleteFileHandler
 */
function deleteFileBackendHandler(name) {
    let userToPass = user // username to hand to backend
    const jsonBody = JSON.stringify({ name, userToPass });
    console.log(jsonBody);

    console.log("file deleted");

    fetch(`http://127.0.0.1:5000/files/${name}`, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json"
        },
        body: jsonBody
    })
        .then((r) => {
            if (!r.ok) {
                throw (new Error(r));
            }
            return (r.json());
        })
        .then((d) => {
            console.log(d);
        })
        .catch((e) => {
            console.error(e);
        });
}

/**
 * Handles the loading of files.
 * Currently unused.
 * @see loadFilesBackendHandler
 */
function loadFilesHandler() {
    console.log("loading files...");
    loadFilesBackendHandler()
        .then((files) => {
            console.log("printing files...");
            for (let i = 0; i < files.length; i++) {
                console.log(files[i]);
            }
            // clear current files
            document.getElementById("file-list").innerHTML = "";
            for (let i = 0; i < files.length; i++) {
                let fileInfo = document.createElement("ol");
                fileInfo.innerHTML = `${files[i].id}: ${files[i].name} - ${files[i].data}`;
                document.getElementById("file-list").appendChild(fileInfo);
            }
        })
        .catch((e) => {
            console.error(e);
            alert("A backend error occurred, try again later.");
        });
}

/**
 * Handles loading permissions.
 * @see loadUsersBackendHandler
 * @see loadPermissionsBackendHandler
 * @see loadFilesBackendHandler
 * @see editPermissionsHandler
 */
function loadPermissionsHandler() {
    console.log("loading permissions...");
    const usersLoaded = loadUsersBackendHandler();
    const permsLoaded = loadPermissionsBackendHandler();
    const filesLoaded = loadFilesBackendHandler();

    // wait for all data to load/resolve
    Promise.all([usersLoaded, permsLoaded, filesLoaded]).then(([users, permissions, files]) => {
        console.log("printing permissions...");
        console.log(permissions);

        // filter permissions by user
        const permissionsByUser = {};
        for (const user in permissions) {
            const user_id = permissions[user].user_id;
            if (!permissionsByUser[user_id]) {
                permissionsByUser[user_id] = [];
            }
            // store permissions by user_id key
            permissionsByUser[user_id].push(permissions[user]);
        }

        // repopulate dropdown
        const dropdown = document.getElementById("user-dropdown");
        for (const user in users) {
            if (!dropdown.querySelector(`option[value="${users[user].id}"]`)) {
                const option = document.createElement("option");
                option.value = users[user].id;
                option.textContent = `${users[user].username}`;
                dropdown.appendChild(option);
            }
        }

        dropdown.addEventListener("change", () => {
            console.log(`user ${dropdown.value} selected`);
            // clear and show table, enable button
            document.getElementById("perms-table-body").innerHTML = "";
            document.getElementById("perms-table").style.display = "block";
            document.querySelector("#edit-perms-form button").removeAttribute("disabled");

            let currentUser = dropdown.value;

            // populate files and permissions
            const tableRow = document.getElementById("perms-table-body");
            for (const file in files) {
                const row = document.createElement("tr");
                row.setAttribute("file", files[file].id);

                // filename
                const name = document.createElement("td");
                name.innerHTML = files[file].name;
                if (files[file].is_file == 0) {
                    name.innerHTML += "/";
                    name.style.fontWeight = "bold";
                }

                // read permission checkbox
                const read = document.createElement("td");
                const readCheckbox = document.createElement("input");
                readCheckbox.type = "checkbox";
                readCheckbox.setAttribute("perm", "read");
                readCheckbox.checked = permissionsByUser[currentUser].find(p => files[file].id === p.file_id && p.r_perm === 1) ? true : false;
                read.appendChild(readCheckbox);

                // write permission checkbox
                const write = document.createElement("td");
                const writeCheckbox = document.createElement("input");
                writeCheckbox.type = "checkbox";
                writeCheckbox.setAttribute("perm", "write");
                writeCheckbox.checked = permissionsByUser[currentUser].find(p => files[file].id === p.file_id && p.w_perm === 1) ? true : false;
                write.appendChild(writeCheckbox);

                // admin permission checkbox
                const admin = document.createElement("td");
                const adminCheckbox = document.createElement("input");
                adminCheckbox.type = "checkbox";
                adminCheckbox.setAttribute("perm", "admin");
                adminCheckbox.checked = permissionsByUser[currentUser].find(p => files[file].id === p.file_id && p.a_perm === 1) ? true : false;
                admin.appendChild(adminCheckbox);

                row.appendChild(name);
                row.appendChild(read);
                row.appendChild(write);
                row.appendChild(admin);
                tableRow.appendChild(row);
            }
        });
    })
        .catch((e) => {
            console.error(e);
            alert("A backend error occurred, try again later.");
        });
}

/**
 * Handles loading users from the backend.
 * @returns {Promise} A promise that resolves with the users from the backend or rejects with an error.
 * @see loadPermissionsHandler
 */
export function loadUsersBackendHandler() {
    return (
        new Promise((resolve, reject) => {
            fetch("http://127.0.0.1:5000/users", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json"
                },
            })
                .then((r) => {
                    if (!r.ok) {
                        throw (new Error(r));
                    }
                    return (r.json());
                })
                .then((d) => {
                    // console.log("data", d);
                    resolve(d.users); // resolves with users
                })
                .catch((e) => {
                    reject(e);
                });
        })
    );
}

/**
 * Handles loading permissions from the backend.
 * @returns {Promise} A promise that resolves with the permissions from the backend or rejects with an error.
 * @see loadPermissionsHandler
 */
export function loadPermissionsBackendHandler() {
    return (
        new Promise((resolve, reject) => {
            fetch("http://127.0.0.1:5000/permissions", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json"
                },
            })
                .then((r) => {
                    if (!r.ok) {
                        throw (new Error(r));
                    }
                    return (r.json());
                })
                .then((d) => {
                    // console.log("data", d);
                    resolve(d.permissions); // resolves with permissions
                })
                .catch((e) => {
                    reject(e);
                });
        })
    );
}

/**
 * Handles loading files from the backend.
 * @returns {Promise} A promise that resolves with the list of files or rejects with an error.
 * @see loadFilesHandler
 */
function loadFilesBackendHandler() {
    return (
        new Promise((resolve, reject) => {
            fetch("http://127.0.0.1:5000/files", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json"
                },
            })
                .then((r) => {
                    if (!r.ok) {
                        throw (new Error(r));
                    }
                    return (r.json());
                })
                .then((d) => {
                    // console.log("data", d);
                    resolve(d.files); // resolves with files
                })
                .catch((e) => {
                    reject(e);
                });
        })
    );
}

/**
 * Handles data from the edit permissions form and resets the form.
 * @param {Event} event The edit permissions form submission event.
 * @see editPermissionsBackendHandler
 */
function editPermissionsHandler(event) {
    event.preventDefault(); // prevents page from reloading

    const updatedPermissions = [];

    // get current user_id from dropdown
    const dropdown = document.getElementById("user-dropdown");
    const user_id = parseInt(dropdown.value);
    // iterate through each row in file permission table
    document.getElementById("perms-table-body").querySelectorAll("tr").forEach(
        (row) => {
            // parse file_id and permissions from elements
            const file_id = parseInt(row.getAttribute("file"));
            const r_perm = row.querySelector("input[perm=read]").checked ? 1 : 0;
            const w_perm = row.querySelector("input[perm=write]").checked ? 1 : 0;
            const a_perm = row.querySelector("input[perm=admin]").checked ? 1 : 0;

            console.log({ user_id, file_id, r_perm, w_perm, a_perm });

            // add to permission list
            updatedPermissions.push({ user_id, file_id, r_perm, w_perm, a_perm });
        }
    );
    alert("User's permissions updated");
    editPermissionsBackendHandler(updatedPermissions);

    visualizeHandler();
}

/**
 * Handles updating permissions on the backend.
 * @param {Array} permissions An array of updated permission objects.
 * @see editPermissionsHandler
 * @see loadPermissionsHandler
 */
function editPermissionsBackendHandler(permissions) {
    let userToPass = user // username to hand to backend
    const jsonBody = JSON.stringify({ permissions, userToPass });

    console.log("permissions updated");

    fetch("http://127.0.0.1:5000/permissions", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: jsonBody
    })
        .then((r) => {
            if (!r.ok) {
                throw (new Error(r));
            }
            return (r.json());
        })
        .then((d) => {
            console.log(d);
            loadPermissionsHandler();
        })
        .catch((e) => {
            console.error(e);
        });
}

export { logoutHandler, newFileHandler, updateFileHandler, deleteFileHandler, loadFilesHandler };