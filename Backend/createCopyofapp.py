"""
This module creates an exact duplicate of app.py called appClone.py with one exception: 
it connects to the test database instead of the actual database
"""

from typing import Dict, Tuple, Any

def clonePythonFile(fileToClone: str, clonedFile: str, lineToModify: int, newLineContent:str) -> None:
    """
    This method creates a modified clone of the input file and writes it to the output file

    Args:
        input_file (str): The path to the input Python file.
        output_file (str): The path to the output Python file where the modified content will be written.
        line_to_modify (int): The line number to modify in the input file (first line is 0).
        new_line_content (str): The new content to replace the specified line in the input file.

    Returns:
        None: This function has no return value.
    """
    with open(fileToClone, 'r') as f:
        lines = f.readlines()

    lines[lineToModify] = newLineContent + '\n'

    with open(clonedFile, 'w') as f:
        f.writelines(lines)

def getLineNumber(file: str, textToFind: str) -> int:
    """
    This method finds the line number of the first occurrence of the specified text in a file.

    Args:
        file (str): The path to the file to search.
        textToFind (str): The text to search for at the beginning of a line.

    Returns:
        int: The line number of the file containing textToFind (first line is 0)
             Returns -1 if not found in the file.
    """
    with open(file, 'r') as f:
        lines = f.readlines()
        for i, line in enumerate(lines):
            if line.strip().startswith(textToFind):
                return i
    return -1  # Return -1 if textToFind is not found

# main logic for this module
inputFile = 'app.py'
outputFile = 'appClone.py'
textToReplace = "app.config['SQLALCHEMY_DATABASE_URI'] ="
lineToModify = getLineNumber(inputFile, textToReplace)
newLineContent = "app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:asdf@localhost/TestDB'"
clonePythonFile(inputFile, outputFile, lineToModify, newLineContent)